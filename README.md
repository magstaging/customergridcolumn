# README #

Add a column to the customer grid

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/CustomerColumn when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

Go to customer grid in the backend and verify the mobile number column is added to the grid